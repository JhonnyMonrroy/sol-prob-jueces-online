import java.util.Scanner;

/**
 * Soluci&oacute;n al problema 617A - Elephant - [https://codeforces.com]
 * 
 * @author JhonnyMonrroy - JhonnyMon (Jhonny Monrroy)
 * @version 1.0 beta - 14 jul. de 2024
 */
// Accepted!!! xD
public class Main {

	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);

		System.out.println((int)Math.ceil(read.nextDouble()/5));

		read.close();
	}

}
